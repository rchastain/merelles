#include "game.h"

void game_quit(t_game *game)
{
  SDL_FreeSurface(game->texture_blue);
  SDL_FreeSurface(game->texture_red);
  SDL_FreeSurface(game->background);
  SDL_FreeSurface(game->screen);
  SDL_DestroyTexture(game->texture);
  SDL_DestroyRenderer(game->renderer);
  SDL_DestroyWindow(game->window);
  TTF_CloseFont(game->font);
  free(game->field);
  free(game);
  TTF_Quit();
  SDL_Quit();
}
