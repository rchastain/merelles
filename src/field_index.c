#include "field.h"

static const int field_coords[48] =
{
  1 * 64, 1 * 64,
  4 * 64, 1 * 64,
  7 * 64, 1 * 64,

  2 * 64, 2 * 64,
  4 * 64, 2 * 64,
  6 * 64, 2 * 64,

  3 * 64, 3 * 64,
  4 * 64, 3 * 64,
  5 * 64, 3 * 64,

  1 * 64, 4 * 64,
  2 * 64, 4 * 64,
  3 * 64, 4 * 64,

  5 * 64, 4 * 64,
  6 * 64, 4 * 64,
  7 * 64, 4 * 64,

  3 * 64, 5 * 64,
  4 * 64, 5 * 64,
  5 * 64, 5 * 64,

  2 * 64, 6 * 64,
  4 * 64, 6 * 64,
  6 * 64, 6 * 64,

  1 * 64, 7 * 64,
  4 * 64, 7 * 64,
  7 * 64, 7 * 64
};

int field_get_index(int x, int y)
{
  int i;

  i = 0;

  while (i < 24)
  {
    if (x > field_coords[i * 2] - IMG_SIZE &&
        x < field_coords[i * 2] + IMG_SIZE &&
        y > field_coords[i * 2 + 1] - IMG_SIZE &&
        y < field_coords[i * 2 + 1] + IMG_SIZE)
      return (i);

    ++i;
  }

  return (-1);
}

void field_get_coords(int idx, SDL_Rect *rect)
{
  rect->x = field_coords[idx * 2] - IMG_SIZE;
  rect->y = field_coords[idx * 2 + 1] - IMG_SIZE;
}
