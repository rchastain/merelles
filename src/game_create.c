#include <SDL2/SDL_image.h>
#include "game.h"
#include "xmalloc.h"

static void init_textures(t_game *game)
{
  game->background = IMG_Load("res/plateau.png");
  game->texture_blue = IMG_Load("res/blanc.png");
  game->texture_red = IMG_Load("res/noir.png");

  if (!game->background || !game->texture_blue || !game->texture_red)
    exit(1);
}

static void init_sdl(t_game *game)
{
  if (SDL_Init(SDL_INIT_VIDEO) == -1)
    exit(1);

  if (TTF_Init() == -1)
    exit(1);

  game->font = TTF_OpenFont("res/alagard.ttf", 16);

  if (!game->font)
    exit(1);

  init_textures(game);
  
  game->screen = SDL_CreateRGBSurface(0, WINDOW_X, WINDOW_Y, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  if (!game->screen)
    exit(1);
  
  game->window = SDL_CreateWindow("Mérelles", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_X, WINDOW_Y, SDL_WINDOW_SHOWN);
  game->renderer = SDL_CreateRenderer(game->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  game->texture = SDL_CreateTextureFromSurface(game->renderer, game->screen);
}

t_game *game_create()
{
  t_game *game;
  game = xmalloc(sizeof(t_game));
  init_sdl(game);
  game->field = xmalloc(sizeof(int) * 24);
  game_init(game);
  return (game);
}
