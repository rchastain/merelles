
uses
  SysUtils, Cairo;

const
  W = 512;
  H = W;
  WOOD = 'wood2.png';
{ https://opengameart.org/content/wood-texture-tiles }

var 
  cr: pcairo_t;
  texture, surface: pcairo_surface_t;
  
begin
  texture := cairo_image_surface_create_from_png(WOOD);
  surface := cairo_image_surface_create(CAIRO_FORMAT_ARGB32, W, H);
  cr := cairo_create(surface);
  
  cairo_set_source_surface(cr, texture, 0, 0);
  cairo_paint(cr);
  
  cairo_scale(cr, W, H);
  
  cairo_set_source_rgb(cr, 0.2, 0.2, 0.2);
  cairo_set_line_width(cr, 0.003);
  
  cairo_rectangle(cr, 1/8, 1/8, 6/8, 6/8);
  cairo_rectangle(cr, 2/8, 2/8, 4/8, 4/8);
  cairo_rectangle(cr, 3/8, 3/8, 2/8, 2/8);

  cairo_move_to(cr, 1/8, 4/8); cairo_line_to(cr, 3/8, 4/8);
  cairo_move_to(cr, 4/8, 1/8); cairo_line_to(cr, 4/8, 3/8);
  cairo_move_to(cr, 5/8, 4/8); cairo_line_to(cr, 7/8, 4/8);
  cairo_move_to(cr, 4/8, 5/8); cairo_line_to(cr, 4/8, 7/8);
  
  cairo_stroke(cr);
  
  cairo_arc(cr, 1/8, 1/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 4/8, 1/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 7/8, 1/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 2/8, 2/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 4/8, 2/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 6/8, 2/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 3/8, 3/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 4/8, 3/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 5/8, 3/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 1/8, 4/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 2/8, 4/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 3/8, 4/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 5/8, 4/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 6/8, 4/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 7/8, 4/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 3/8, 5/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 4/8, 5/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 5/8, 5/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 2/8, 6/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 4/8, 6/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 6/8, 6/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 1/8, 7/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 4/8, 7/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  cairo_arc(cr, 7/8, 7/8, 0.007, 0, 2 * PI); cairo_fill(cr);
  
  cairo_surface_write_to_png(surface, pchar(ChangeFileExt({$I %FILE%}, '.png')));
  
  cairo_destroy(cr);
  cairo_surface_destroy(surface);
end.
