# Mérelles

Implémentation du jeu des [mérelles](https://www.littre.org/definition/marelle) en C, avec la bibliothèque SDL.

## Ressources

* Police : [Alagard](https://www.dafont.com/fr/alagard.font)
* Textures : [Wood Texture Tiles](https://opengameart.org/content/wood-texture-tiles)

## Auteur

L'auteur de la [version originale](https://github.com/paul-maxime/merreles) du programme est [Paul-Maxime](https://github.com/paul-maxime).

La [présente version](https://gitlab.com/rchastain/merelles) ne diffère de la version originale que par la taille de la fenêtre, les images utilisées pour représenter le plateau et les pions, et d'autres petites choses sans importance.

## Capture d'écran

![Capture d'écran](capture-mageia.png)
